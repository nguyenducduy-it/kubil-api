import { Module, Global } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { SistemassService } from "../sistemass/sistemass.service";
import { GalleriesService } from "../galleries/galleries.service";
import { SistemassResolver } from "../sistemass/sistemass.resolver";
import { GalleriesResolver } from "../galleries/galleries.resolver";
import { Sistemas } from "../../entities/sistemas.entity";
import { Gallery } from "../../entities/gallery.entity";
import { MobileResolver } from "./mobile.resolver";
import { ConfigService } from "../config.service";
import { config } from "../app.config";

@Global()
@Module({
    imports: [
        TypeOrmModule.forFeature([Sistemas]),
        TypeOrmModule.forFeature([Gallery])
    ],
    exports: [
        SistemassService,
        GalleriesService
    ],
    providers: [
        SistemassService,
        GalleriesService,
        SistemassResolver,
        GalleriesResolver,
        MobileResolver,
        { provide: ConfigService, useValue: new ConfigService(config) }
    ]
})
export class MobileModule { }
