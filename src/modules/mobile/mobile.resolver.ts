import { UseInterceptors, UseGuards } from "@nestjs/common";
import { Query, Mutation, Resolver, ResolveProperty } from "@nestjs/graphql";
import { SistemassService } from "../sistemass/sistemass.service";
import { GalleriesService } from "../galleries/galleries.service";
import { Sistemas } from "../../entities/sistemas.entity";
import { Gallery } from "../../entities/gallery.entity";
import { plainToClass } from "class-transformer";
import { SistemasMobileTransformInterceptor } from "../../interceptors/sistemas-mobile-transform.interceptor";
import { GalleryMobileTransformInterceptor } from "../../interceptors/gallery-mobile-transform.interceptor";

@Resolver("Mobile")
export class MobileResolver {
    constructor(
        private readonly sistemassService: SistemassService,
        private readonly galleriesService: GalleriesService
    ) {}

    @Query("getMobileSistemass")
    @UseInterceptors(new SistemasMobileTransformInterceptor())
    async getMobileSistemass(_: any, { opts }) {
        try {
            const mySistemass = await this.sistemassService.findAll({
                curPage: opts.curPage,
                perPage: opts.perPage,
                q: opts.q,
                sort: opts.sort
            });
            return {
                items: plainToClass(Sistemas, mySistemass.items),
                meta: mySistemass.meta
            };
        } catch (error) {
            throw error;
        }
    }

    @Query("getMobileSistemas")
    @UseInterceptors(new SistemasMobileTransformInterceptor())
    async getMobileSistemas(_: any, { slug }) {
        try {
            const mySistemas = await this.sistemassService.findOneBySlug(slug);
            return plainToClass(Sistemas, mySistemas);
        } catch (error) {
            throw error;
        }
    }

    @Query("getMobileGalleries")
    @UseInterceptors(new GalleryMobileTransformInterceptor())
    async getMobileGalleries(_: any, { opts }) {
        try {
            const myGalleries = await this.galleriesService.findAll({
                curPage: opts.curPage,
                perPage: opts.perPage,
                q: opts.q,
                sort: opts.sort
            });
            return {
                items: plainToClass(Gallery, myGalleries.items),
                meta: myGalleries.meta
            };
        } catch (error) {
            throw error;
        }
    }
}
