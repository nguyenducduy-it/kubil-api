import { Module, Global } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { SistemassService } from "./sistemass.service";
import { SistemassResolver } from "./sistemass.resolver";
import { Sistemas } from "../../entities/sistemas.entity";
import { ConfigService } from "../config.service";
import { config } from "../app.config";

@Module({
    imports: [TypeOrmModule.forFeature([Sistemas])],
    exports: [SistemassService],
    providers: [SistemassService, SistemassResolver, { provide: ConfigService, useValue: new ConfigService(config) }]
})
export class SistemassModule {}
