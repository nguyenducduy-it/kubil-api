import { Injectable, BadRequestException } from "@nestjs/common";
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { Sistemas } from "../../entities/sistemas.entity";
import { UserException } from "../../filters/error/user-exception.error";
import * as slug from "slug";

@Injectable()
export class SistemassService {
    constructor(@InjectRepository(Sistemas) private readonly sistemasRepository: Repository<Sistemas>) { }

    async findAll(options: { curPage: number; perPage: number; q?: string; group?: number; sort?: string }) {
        try {
            let objects: [Sistemas[], number];
            let qb = this.sistemasRepository.createQueryBuilder("sistemas");

            // if (options.q) {
            //     qb = qb.where("user.fullName like :q or user.email like :q or user.id = :id", {
            //         q: `%${options.q}%`,
            //         id: options.q
            //     });
            // }

            // sort
            options.sort =
                options.sort && new Sistemas().hasOwnProperty(options.sort.replace("-", "")) ? options.sort : "-id";
            const field = options.sort.replace("-", "");
            if (options.sort) {
                if (options.sort[0] === "-") {
                    qb = qb.addOrderBy("sistemas." + field, "DESC");
                } else {
                    qb = qb.addOrderBy("sistemas." + field, "ASC");
                }
            }

            // offset & limit
            qb = qb.skip((options.curPage - 1) * options.perPage).take(options.perPage);

            // run query
            objects = await qb.getManyAndCount();

            return {
                items: objects[0],
                meta: {
                    curPage: options.curPage,
                    perPage: options.perPage,
                    totalPages: options.perPage > objects[1] ? 1 : Math.ceil(objects[1] / options.perPage),
                    totalResults: objects[1]
                }
            };
        } catch (error) {
            throw error;
        }
    }

    async findOne(id: number) {
        try {
            return await this.sistemasRepository.findOneOrFail(id);
        } catch (error) {
            throw error;
        }
    }

    async findOneBySlug(slug: string) {
        try {
            return await this.sistemasRepository.findOneOrFail({
                where: {
                    slug: slug
                }
            });
        } catch (error) {
            throw error;
        }
    }

    async create(formData: Sistemas) {
        try {
            let keyword = [];

            for (let index = 0; index < formData.keyword.length; index++) {
                const element = formData.keyword[index];
                keyword.push(element['text']);
            }
            formData.keyword = keyword.join(",");
            formData.slug = slug(formData.title, { lovwer: true });

            const mySistemas = this.sistemasRepository.create(formData);
            return await this.sistemasRepository.save(mySistemas);
        } catch (error) {
            throw error;
        }
    }

    async update(id: number, formData: any) {
        try {
            let mySistemas = await this.sistemasRepository.findOneOrFail(id);

            mySistemas.slug = slug(formData.title, { lovwer: true });
            mySistemas.title = formData.title;
            mySistemas.description = formData.description;
            mySistemas.content = formData.content;
            mySistemas.lang = formData.lang;
            mySistemas.keyword = formData.keyword;
            mySistemas.order = formData.order;
            mySistemas.status = formData.status;
            mySistemas.coverPath = formData.coverPath;

            return await this.sistemasRepository.save(mySistemas);
        } catch (error) {
            throw error;
        }
    }

    async delete(id: number) {
        try {
            await this.sistemasRepository.findOneOrFail(id);
            return await this.sistemasRepository.delete(id);
        } catch (error) {
            throw error;
        }
    }

    async changeStatus(id: number, status: number) {
        try {
            let mySistemas = await this.sistemasRepository.findOneOrFail(id);
            mySistemas.status = status;

            await this.sistemasRepository.save(mySistemas);
            return await this.sistemasRepository.findOneOrFail({
                where: { id: id }
            });
        } catch (error) {
            throw error;
        }
    }
}
