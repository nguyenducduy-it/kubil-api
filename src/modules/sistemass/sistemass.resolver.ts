import { UseInterceptors, UseGuards } from "@nestjs/common";
import { Query, Mutation, Resolver, ResolveProperty } from "@nestjs/graphql";
import { RavenInterceptor } from "nest-raven";
import { AuthGuard } from "../../guards/auth.guard";
import { Roles } from "../../decorators/roles.decorator";
import { SistemassService } from "./sistemass.service";
import { Sistemas } from "../../entities/sistemas.entity";
import { plainToClass } from "class-transformer";
import { SistemasTransformInterceptor } from "../../interceptors/sistemas-transform.interceptor";

@Resolver("Sistemas")
@UseGuards(AuthGuard)
export class SistemassResolver {
    constructor(private readonly sistemassService: SistemassService) {}

    @Query("getSistemass")
    @Roles("isSuperUser")
    @UseInterceptors(new SistemasTransformInterceptor())
    async getSistemass(_: any, { opts }) {
        try {
            const mySistemass = await this.sistemassService.findAll({
                curPage: opts.curPage,
                perPage: opts.perPage,
                q: opts.q,
                sort: opts.sort
            });
            return {
                items: plainToClass(Sistemas, mySistemass.items),
                meta: mySistemass.meta
            };
        } catch (error) {
            throw error;
        }
    }

    @Query("getSistemas")
    @Roles("isSuperUser")
    @UseInterceptors(new SistemasTransformInterceptor())
    async getSistemas(_: any, { id }) {
        try {
            const mySistemas = await this.sistemassService.findOne(id);
            return plainToClass(Sistemas, mySistemas);
        } catch (error) {
            throw error;
        }
    }

    @Mutation("addSistemas")
    @Roles("isSuperUser")
    @UseInterceptors(new SistemasTransformInterceptor())
    async addSistemas(_: any, { input }) {
        try {
            return await this.sistemassService.create(input);
        } catch (error) {
            throw error;
        }
    }

    @Mutation("updateSistemas")
    @Roles("isSuperUser")
    @UseInterceptors(new SistemasTransformInterceptor())
    async updateSistemas(_: any, { id, input }) {
        try {
            return await this.sistemassService.update(id, input);
        } catch (error) {
            throw error;
        }
    }

    @Mutation("deleteSistemas")
    @Roles("isSuperUser")
    async deleteSistemas(_: any, { id, input }) {
        try {
            return await this.sistemassService.delete(id);
        } catch (error) {
            throw error;
        }
    }

    @Mutation("changeSistemasStatus")
    @Roles("isSuperUser")
    @UseInterceptors(new SistemasTransformInterceptor())
    async changeSistemasStatus(_: any, { id, status }) {
        try {
            return await this.sistemassService.changeStatus(id, status);
        } catch (error) {
            throw error;
        }
    }
}
