import { Injectable, BadRequestException } from "@nestjs/common";
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { Gallery } from "../../entities/gallery.entity";
import { UserException } from "../../filters/error/user-exception.error";
import * as slug from "slug";

@Injectable()
export class GalleriesService {
    constructor(@InjectRepository(Gallery) private readonly galleryRepository: Repository<Gallery>) {}

    async findAll(options: { curPage: number; perPage: number; q?: string; group?: number; sort?: string }) {
        try {
            let objects: [Gallery[], number];
            let qb = this.galleryRepository.createQueryBuilder("gallery");

            // if (options.q) {
            //     qb = qb.where("user.fullName like :q or user.email like :q or user.id = :id", {
            //         q: `%${options.q}%`,
            //         id: options.q
            //     });
            // }

            // sort
            options.sort =
                options.sort && new Gallery().hasOwnProperty(options.sort.replace("-", "")) ? options.sort : "-id";
            const field = options.sort.replace("-", "");
            if (options.sort) {
                if (options.sort[0] === "-") {
                    qb = qb.addOrderBy("gallery." + field, "DESC");
                } else {
                    qb = qb.addOrderBy("gallery." + field, "ASC");
                }
            }

            // offset & limit
            qb = qb.skip((options.curPage - 1) * options.perPage).take(options.perPage);

            // run query
            objects = await qb.getManyAndCount();

            return {
                items: objects[0],
                meta: {
                    curPage: options.curPage,
                    perPage: options.perPage,
                    totalPages: options.perPage > objects[1] ? 1 : Math.ceil(objects[1] / options.perPage),
                    totalResults: objects[1]
                }
            };
        } catch (error) {
            throw error;
        }
    }

    async findOne(id: number) {
        try {
            return await this.galleryRepository.findOneOrFail(id);
        } catch (error) {
            throw error;
        }
    }

    async create(formData: Gallery) {
        try {
            let keyword = [];

            for (let index = 0; index < formData.keyword.length; index++) {
                const element = formData.keyword[index];
                keyword.push(element["text"]);
            }
            formData.keyword = keyword.join(",");

            const myGallery = this.galleryRepository.create(formData);
            return await this.galleryRepository.save(myGallery);
        } catch (error) {
            throw error;
        }
    }

    async update(id: number, formData: any) {
        try {
            let myGallery = await this.galleryRepository.findOneOrFail(id);

            myGallery.caption = formData.caption;
            myGallery.path = formData.path;
            myGallery.keyword = formData.keyword;
            myGallery.status = formData.status;
            myGallery.type = formData.type;

            return await this.galleryRepository.save(myGallery);
        } catch (error) {
            throw error;
        }
    }

    async delete(id: number) {
        try {
            await this.galleryRepository.findOneOrFail(id);
            return await this.galleryRepository.delete(id);
        } catch (error) {
            throw error;
        }
    }

    async changeStatus(id: number, status: number) {
        try {
            let myGallery = await this.galleryRepository.findOneOrFail(id);
            myGallery.status = status;

            await this.galleryRepository.save(myGallery);
            return await this.galleryRepository.findOneOrFail({ where: { id: id } });
        } catch (error) {
            throw error;
        }
    }
}
