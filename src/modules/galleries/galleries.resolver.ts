import { UseInterceptors, UseGuards } from "@nestjs/common";
import { Query, Mutation, Resolver, ResolveProperty } from "@nestjs/graphql";
import { AuthGuard } from "../../guards/auth.guard";
import { Roles } from "../../decorators/roles.decorator";
import { GalleriesService } from "./galleries.service";
import { Gallery } from "../../entities/gallery.entity";
import { plainToClass } from "class-transformer";
import { GalleryTransformInterceptor } from "../../interceptors/gallery-transform.interceptor";

@Resolver("Galleries")
@UseGuards(AuthGuard)
export class GalleriesResolver {
    constructor(private readonly galleriesService: GalleriesService) {}

    @Query("getGalleries")
    @Roles("isSuperUser")
    @UseInterceptors(new GalleryTransformInterceptor())
    async getGalleries(_: any, { opts }) {
        try {
            const myGalleries = await this.galleriesService.findAll({
                curPage: opts.curPage,
                perPage: opts.perPage,
                q: opts.q,
                sort: opts.sort
            });
            return {
                items: plainToClass(Gallery, myGalleries.items),
                meta: myGalleries.meta
            };
        } catch (error) {
            throw error;
        }
    }

    @Query("getGallery")
    @Roles("isSuperUser")
    @UseInterceptors(new GalleryTransformInterceptor())
    async getGallery(_: any, { id }) {
        try {
            const myGallerys = await this.galleriesService.findOne(id);
            return plainToClass(Gallery, myGallerys);
        } catch (error) {
            throw error;
        }
    }

    @Mutation("addGallery")
    @Roles("isSuperUser")
    @UseInterceptors(new GalleryTransformInterceptor())
    async addGallery(_: any, { input }) {
        try {
            return await this.galleriesService.create(input);
        } catch (error) {
            throw error;
        }
    }

    @Mutation("updateGallery")
    @Roles("isSuperUser")
    @UseInterceptors(new GalleryTransformInterceptor())
    async updateGallery(_: any, { id, input }) {
        try {
            return await this.galleriesService.update(id, input);
        } catch (error) {
            throw error;
        }
    }

    @Mutation("deleteGallery")
    @Roles("isSuperUser")
    async deleteGallery(_: any, { id, input }) {
        try {
            return await this.galleriesService.delete(id);
        } catch (error) {
            throw error;
        }
    }

    @Mutation("changeGalleryStatus")
    @Roles("isSuperUser")
    @UseInterceptors(new GalleryTransformInterceptor())
    async changeGalleryStatus(_: any, { id, status }) {
        try {
            return await this.galleriesService.changeStatus(id, status);
        } catch (error) {
            throw error;
        }
    }
}
