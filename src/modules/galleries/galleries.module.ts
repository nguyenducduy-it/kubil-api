import { Module, Global } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { GalleriesService } from "./galleries.service";
import { GalleriesResolver } from "./galleries.resolver";
import { Gallery } from "../../entities/gallery.entity";
import { ConfigService } from "../config.service";
import { config } from "../app.config";

@Module({
    imports: [TypeOrmModule.forFeature([Gallery])],
    exports: [GalleriesService],
    providers: [
        GalleriesService,
        GalleriesResolver,
        { provide: ConfigService, useValue: new ConfigService(config) }
    ]
})
export class GalleriesModule {}
