import { Entity, Column, PrimaryGeneratedColumn, BeforeInsert, BeforeUpdate, BaseEntity } from "typeorm";
import { IsNotEmpty, IsInt } from "class-validator";

enum Status {
    PUBLISH = <number>1,
    DRAFT = <number>3
}

@Entity({ name: "sistemas" })
export class Sistemas extends BaseEntity {
    @PrimaryGeneratedColumn({ name: "sis_id" })
    public id: number = 0;

    @IsNotEmpty()
    @Column({ name: "sis_title" })
    public title: string = "";

    @Column({ name: "sis_description" })
    public description: string = "";

    @Column({ name: "sis_slug" })
    public slug: string = "";

    @Column({ name: "sis_content" })
    public content: string = "";

    @Column({ name: "sis_lang" })
    public lang: string = "";

    @Column({ name: "sis_keyword" })
    public keyword: string = "";

    @Column({ name: "sis_status" })
    public status: number = 0;

    @Column({ name: "sis_order" })
    public order: number = 0;

    @Column({ name: "sis_cover_path" })
    public coverPath: string = "";

    @Column({ name: "sis_date_created" })
    public dateCreated: number = 0;

    @Column({ name: "sis_date_modified" })
    public dateModified: number = 0;

    @BeforeInsert()
    private async doBeforeInsertion() {
        this.dateCreated = Math.floor(Date.now() / 1000);
    }

    @BeforeUpdate()
    private async doBeforeUpdate() {
        this.dateModified = Math.floor(Date.now() / 1000);
    }

    public getStatusName() {
        let name: string = "";

        switch (this.status) {
            case Status.PUBLISH:
                name = "Publish";
                break;
            case Status.DRAFT:
                name = "Draft";
                break;
        }

        return name;
    }
}
