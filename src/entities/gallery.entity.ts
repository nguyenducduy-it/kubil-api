import { Entity, Column, PrimaryGeneratedColumn, BeforeInsert, BeforeUpdate, BaseEntity } from "typeorm";
import { IsNotEmpty, IsInt } from "class-validator";

enum Status {
    ENABLED = <number>1,
    DISABLED = <number>3
}

enum Type {
    PHOTO = <number>1,
    VIDEO = <number>3
}

enum Partner {
    LOCAL = <number>1,
    YOUTUBE = <number>3,
    VIMEO = <number>5
}

@Entity({ name: "gallery" })
export class Gallery extends BaseEntity {
    @PrimaryGeneratedColumn({ name: "gal_id" })
    public id: number = 0;

    @IsNotEmpty()
    @Column({ name: "gal_caption" })
    public caption: string = "";

    @Column({ name: "gal_path" })
    public path: string = "";

    @Column({ name: "gal_thumbnail" })
    public thumbnail: string = "";

    @Column({ name: "gal_status" })
    public status: number = 0;

    @Column({ name: "gal_keyword" })
    public keyword: string = "";

    @Column({ name: "gal_type" })
    public type: number = 0;

    @Column({ name: "gal_partner" })
    public partner: number = 0;

    @Column({ name: "gal_date_created" })
    public dateCreated: number = 0;

    @Column({ name: "gal_date_modified" })
    public dateModified: number = 0;

    @BeforeInsert()
    private async doBeforeInsertion() {
        this.dateCreated = Math.floor(Date.now() / 1000);
    }

    @BeforeUpdate()
    private async doBeforeUpdate() {
        this.dateModified = Math.floor(Date.now() / 1000);
    }

    public getStatusName() {
        let name: string = "";

        switch (this.status) {
            case Status.ENABLED:
                name = "Enabled";
                break;
            case Status.DISABLED:
                name = "Disabled";
                break;
        }

        return name;
    }

    public getTypeName() {
        let name: string = "";

        switch (this.type) {
            case Type.PHOTO:
                name = "Photo";
                break;
            case Type.VIDEO:
                name = "Video";
                break;
        }

        return name;
    }

    public getPartnerName() {
        let name: string = "";

        switch (this.partner) {
            case Partner.LOCAL:
                name = "Local";
                break;
            case Partner.YOUTUBE:
                name = "Youtube";
                break;
            case Partner.VIMEO:
                name = "Vimeo";
                break;
        }

        return name;
    }
}
