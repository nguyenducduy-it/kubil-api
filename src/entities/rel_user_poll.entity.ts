import { Entity, Column, PrimaryGeneratedColumn, BeforeInsert, BeforeUpdate, BaseEntity } from "typeorm";
import { IsNotEmpty, IsInt } from "class-validator";

@Entity({ name: "poll" })
export class RelUserPoll extends BaseEntity {
    @PrimaryGeneratedColumn({ name: "rup_id" })
    public id: number = 0;

    @IsNotEmpty()
    @Column({ name: "u_id" })
    public uid: number = 0;

    @IsNotEmpty()
    @Column({ name: "po_id" })
    public poid: number = 0;
}
