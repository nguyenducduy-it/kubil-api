import { Entity, Column, PrimaryGeneratedColumn, BeforeInsert, BeforeUpdate, BaseEntity } from "typeorm";
import { IsNotEmpty, IsInt } from "class-validator";

enum Status {
    ENABLED = <number>1,
    DISABLED = <number>3
}

@Entity({ name: "poll" })
export class Poll extends BaseEntity {
    @PrimaryGeneratedColumn({ name: "po_id" })
    public id: number = 0;

    @IsNotEmpty()
    @Column({ name: "po_content" })
    public content: string = "";

    @Column({ name: "po_status" })
    public status: number = 0;

    @Column({ name: "po_date_created" })
    public dateCreated: number = 0;

    @Column({ name: "po_date_modified" })
    public dateModified: number = 0;

    @BeforeInsert()
    private async doBeforeInsertion() {
        this.dateCreated = Math.floor(Date.now() / 1000);
    }

    @BeforeUpdate()
    private async doBeforeUpdate() {
        this.dateModified = Math.floor(Date.now() / 1000);
    }

    public getStatusName() {
        let name: string = "";

        switch (this.status) {
            case Status.ENABLED:
                name = "Enabled";
                break;
            case Status.DISABLED:
                name = "Disabled";
                break;
        }

        return name;
    }
}
