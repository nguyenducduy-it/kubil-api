import { Injectable, NestInterceptor, ExecutionContext } from "@nestjs/common";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import * as moment from "moment";

export interface Response<T> {
    data: T;
}

@Injectable()
export class SistemasTransformInterceptor<T> implements NestInterceptor<T, Response<T>> {
    intercept(context: ExecutionContext, call$: Observable<T>): Observable<any> {
        return call$.pipe(
            tap(async data => {
                if (typeof data.items !== "undefined") {
                    data.items.map(sistemas => this._transform(sistemas));
                } else {
                    data = this._transform(data);
                }

                return data;
            })
        );
    }

    private _transform(sistemas) {
        sistemas.status = {
            label: sistemas.getStatusName(),
            value: sistemas.status
        };
        sistemas.dateCreated = {
            readable: moment.unix(sistemas.dateCreated).format("MMM Do YYYY"),
            timestamp: sistemas.dateCreated
        };
    }
}
