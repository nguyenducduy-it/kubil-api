import { Injectable, NestInterceptor, ExecutionContext } from "@nestjs/common";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import * as moment from "moment";

export interface Response<T> {
    data: T;
}

@Injectable()
export class GalleryTransformInterceptor<T> implements NestInterceptor<T, Response<T>> {
    intercept(context: ExecutionContext, call$: Observable<T>): Observable<any> {
        return call$.pipe(
            tap(async data => {
                if (typeof data.items !== "undefined") {
                    data.items.map(gallery => this._transform(gallery));
                } else {
                    data = this._transform(data);
                }

                return data;
            })
        );
    }

    private _transform(gallery) {
        gallery.status = {
            label: gallery.getStatusName(),
            value: gallery.status
        };
        gallery.partner = {
            label: gallery.getPartnerName(),
            value: gallery.partner
        };
        gallery.type = {
            label: gallery.getTypeName(),
            value: gallery.type
        };
        gallery.dateCreated = {
            readable: moment.unix(gallery.dateCreated).format("MMM Do YYYY"),
            timestamp: gallery.dateCreated
        };
    }
}
